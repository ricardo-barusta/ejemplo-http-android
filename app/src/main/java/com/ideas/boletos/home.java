package com.ideas.boletos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.ideas.boletos.api.NIApiService;

import com.ideas.boletos.api.apiConstants;
import com.ideas.boletos.api.models.folioRequest;
import com.ideas.boletos.api.models.folioResponse;

import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;




import retrofit2.Call;
import retrofit2.converter.gson.GsonConverterFactory;

public class home extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    public Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        FloatingActionButton cameraButton = (FloatingActionButton) findViewById(R.id.fab2);
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new IntentIntegrator((Activity) view.getContext()).initiateScan();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText f = (EditText)findViewById(R.id.folio_txt);
                if (f.getText().toString().equals("")){
                    Snackbar.make(view, "FOLIO VACIO!!", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }else{
                    Intent intent = getIntent();
                    String t = intent.getStringExtra("TOKEN");
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(apiConstants.URL_BASE)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    final NIApiService service = retrofit.create(NIApiService.class);

                    folioRequest folioRequest = new folioRequest();

                    folioRequest.setFolio(f.getText().toString());

                    Call<folioResponse> folioResponseCall = service.folio(folioRequest, t);
                    folioResponseCall.enqueue(new Callback<folioResponse>() {
                        @Override
                        public void onResponse(Call<folioResponse> call, Response<folioResponse> response) {
                            folioResponse folioResponse = response.body();
                            if (folioResponse == null) {
                                Toast.makeText(context, "No hay datos", Toast.LENGTH_SHORT).show();
                            } else {
                                Log.d("RESPONSE", folioResponse.getFolio());
                                // Toast.makeText(context, folioResponse.getFolio(), Toast.LENGTH_SHORT).show();
                                Intent a = new Intent(context, Boleto.class);
                                a.putExtra("folio", folioResponse.getFolio());
                                a.putExtra("operador", folioResponse.getOperador());
                                a.putExtra("economico", folioResponse.getEconomico());
                                a.putExtra("puerta", folioResponse.getPuerta());
                                a.putExtra("fechahora", folioResponse.getFechahora());
                                a.putExtra("zona", folioResponse.getZona());
                                a.putExtra("tarifa", folioResponse.getTarifa());
                                a.putExtra("servicio", folioResponse.getServicio());
                                startActivity(a);


                            }
                        }

                        @Override
                        public void onFailure(Call<folioResponse> call, Throwable t) {
                            Toast.makeText(context, "ERROR EN EL SERVIDOR", Toast.LENGTH_SHORT).show();
                        }
                    });

                }

            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }



    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "home Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.ideas.boletos/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "home Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.ideas.boletos/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        String fol;
        EditText folio = (EditText)findViewById(R.id.folio_txt);
        Intent intent = getIntent();
        String token = intent.getStringExtra("TOKEN");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiConstants.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final NIApiService service = retrofit.create(NIApiService.class);

        if (result != null) {
            if (result==null){
                Log.d("DATOS","Scan cancelado");
                Toast.makeText(this,"Cancelado",Toast.LENGTH_SHORT).show();
            }else {

                if (result.getContents() == null) {
                    Toast.makeText(this, "Cancelado", Toast.LENGTH_SHORT).show();
                }else{
                Log.d("MainActivity", "Scanned" + result.getContents());
                fol = result.getContents();

                folioRequest folioRequest = new folioRequest();

                folioRequest.setFolio(fol);

                Call<folioResponse> folioResponseCall = service.folio(folioRequest, token);
                folioResponseCall.enqueue(new Callback<folioResponse>() {
                    @Override
                    public void onResponse(Call<folioResponse> call, Response<folioResponse> response) {
                        folioResponse folioResponse = response.body();
                        if (folioResponse == null) {
                            Toast.makeText(context, "No hay datos", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.d("RESPONSE", folioResponse.getFolio());
                            // Toast.makeText(context, folioResponse.getFolio(), Toast.LENGTH_SHORT).show();
                            Intent a = new Intent(context, Boleto.class);
                            a.putExtra("folio", folioResponse.getFolio());
                            a.putExtra("operador", folioResponse.getOperador());
                            a.putExtra("economico", folioResponse.getEconomico());
                            a.putExtra("puerta", folioResponse.getPuerta());
                            a.putExtra("fechahora", folioResponse.getFechahora());
                            a.putExtra("zona", folioResponse.getZona());
                            a.putExtra("tarifa", folioResponse.getTarifa());
                            a.putExtra("servicio", folioResponse.getServicio());
                            startActivity(a);


                        }
                    }

                    @Override
                    public void onFailure(Call<folioResponse> call, Throwable t) {
                        Toast.makeText(context, "ERROR EN EL SERVIDOR", Toast.LENGTH_SHORT).show();
                    }
                });
            }

                //Toast.makeText(this, token,Toast.LENGTH_LONG).show();;


            }//cierre de else para nulls


        }
       /* if (scaningResult!=null){
            folio.setText(data.getStringExtra("SCAN_RESULT"));
        }*/else{
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
