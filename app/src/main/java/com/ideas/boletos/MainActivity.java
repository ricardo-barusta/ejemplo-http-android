package com.ideas.boletos;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.ideas.boletos.api.NIAdapter;
import com.ideas.boletos.api.NIApiService;
import com.ideas.boletos.api.apiConstants;
import com.ideas.boletos.api.models.tokenRequest;
import com.ideas.boletos.api.models.tokenResponse;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    EditText username, password;



    public static final String TOKEN_PREFERENCES = "token";



    public String getToken(){
        SharedPreferences shared = getSharedPreferences(TOKEN_PREFERENCES, MODE_PRIVATE);
        String channel = (shared.getString("TOKEN", ""));
        return channel;
    }
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiConstants.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final NIApiService service = retrofit.create(NIApiService.class);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {



                username = (EditText) findViewById(R.id.username_et);
                password = (EditText) findViewById(R.id.password_et);
                tokenRequest tokenRequest = new tokenRequest();
                tokenRequest.setUsername(username.getText().toString());
                tokenRequest.setPassword(password.getText().toString());

                Call<tokenResponse> tokenResponseCall = service.auth(tokenRequest);
                tokenResponseCall.enqueue(new Callback<tokenResponse>() {
                    @Override
                    public void onResponse(Call<tokenResponse> call, Response<tokenResponse> response) {
                        int statusCode = response.code();

                        tokenResponse tokenResponse = response.body();
                        if (tokenResponse.getLogin()==true){
                            Snackbar.make(view, "INICIO DE SESION EXITOSO", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();

                           Intent a = new Intent(view.getContext(),home.class);
                            a.putExtra("TOKEN", tokenResponse.getToken());

                            SharedPreferences settings = getSharedPreferences(TOKEN_PREFERENCES, MODE_PRIVATE);
                            SharedPreferences.Editor prefEditor = settings.edit();
                            prefEditor.putString("TOKEN", tokenResponse.getToken());
                            prefEditor.commit();

                            startActivity(a);
                        }else{
                            Snackbar.make(view, "CONTRASEÑA O USUARIO ERRONEOS", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<tokenResponse> call, Throwable throwable) {
                        Snackbar.make(view, "ERROR", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                });



                /*Snackbar.make(view, "Usuario: "+username.getText().toString()+"  password: "+password.getText().toString(), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.ideas.boletos/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.ideas.boletos/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
