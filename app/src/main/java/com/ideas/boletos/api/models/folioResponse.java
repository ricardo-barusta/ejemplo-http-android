package com.ideas.boletos.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rich on 12/04/2016.
 */
public class folioResponse {


    @SerializedName("folio")
    @Expose
    private String folio;

    @SerializedName("operador")
    @Expose
    private String operador;

    @SerializedName("economico")
    @Expose
    private String economico;

    @SerializedName("puerta")
    @Expose
    private int puerta;

    @SerializedName("fechahora")
    @Expose
    private String fechahora;

    @SerializedName("zona")
    @Expose
    private String zona;

    @SerializedName("tarifa")
    @Expose
    private String tarifa;


    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getTarifa() {
        return tarifa;
    }

    public void setTarifa(String tarifa) {
        this.tarifa = tarifa;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getFechahora() {
        return fechahora;
    }

    public void setFechahora(String fechahora) {
        this.fechahora = fechahora;
    }

    public int getPuerta() {
        return puerta;
    }

    public void setPuerta(int puerta) {
        this.puerta = puerta;
    }

    public String getEconomico() {
        return economico;
    }

    public void setEconomico(String economico) {
        this.economico = economico;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    private String servicio;


}
