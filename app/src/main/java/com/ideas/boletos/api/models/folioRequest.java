package com.ideas.boletos.api.models;

/**
 * Created by Rich on 12/04/2016.
 */
public class folioRequest {

    private String folio;

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }
}
