package com.ideas.boletos.api;



import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;

import com.ideas.boletos.MainActivity;
import com.ideas.boletos.api.models.folioRequest;
import com.ideas.boletos.api.models.folioResponse;
import com.ideas.boletos.api.models.tokenRequest;
import com.ideas.boletos.api.models.tokenResponse;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;


/**
 * Created by Rich on 08/04/2016.
 */
public interface NIApiService {



    /*@GET(apiConstants.URL_BASE+apiConstants.PATH_USERS)
    public void getusers(Call<List<>>)*/


    @POST("/auth")
    Call<tokenResponse> auth( @Body tokenRequest tokenRequest);



    @POST(apiConstants.PATH_DATA)
    Call<folioResponse> folio(@Body folioRequest folioRequest,@Header("Authorization") String contentRange);


}


