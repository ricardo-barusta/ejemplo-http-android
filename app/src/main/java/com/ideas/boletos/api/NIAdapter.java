package com.ideas.boletos.api;

import retrofit2.Retrofit;

/**
 * Created by Rich on 09/04/2016.
 */
public class NIAdapter {
    private static NIApiService API_SERVICE;
    public static NIApiService getApiService(){
        if (API_SERVICE==null){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(apiConstants.URL_BASE)
                    .build();

            API_SERVICE = retrofit.create(NIApiService.class);

        }
        return API_SERVICE;
    }
}
