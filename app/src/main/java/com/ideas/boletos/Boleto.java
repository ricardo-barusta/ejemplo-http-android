package com.ideas.boletos;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.util.Hashtable;

public class Boleto extends AppCompatActivity {

    public static Bitmap createBarCode (String codeData, int codeHeight, int codeWidth) {

        try {
            Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<EncodeHintType, ErrorCorrectionLevel>();
            hintMap.put (EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

            QRCodeWriter codeWriter;
            codeWriter =new  QRCodeWriter();


            BitMatrix byteMatrix = codeWriter.encode (
                    codeData,
                    BarcodeFormat.QR_CODE,
                    codeWidth,
                    codeHeight,
                    hintMap
            );

            int width   = byteMatrix.getWidth ();
            int height  = byteMatrix.getHeight ();

            Bitmap imageBitmap = Bitmap.createBitmap (width, height, Bitmap.Config.ARGB_8888);

            for (int i = 0; i < width; i ++) {
                for (int j = 0; j < height; j ++) {
                    imageBitmap.setPixel (i, j, byteMatrix.get (i, j) ? Color.BLACK: Color.WHITE);
                }
            }

            return imageBitmap;

        } catch (WriterException e) {
            e.printStackTrace ();
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boleto);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Intent intent = getIntent();

        String foli = intent.getStringExtra("folio")+"%?"+intent.getStringExtra("economico")+"%"+intent.getStringExtra("operador")+"%"+intent.getStringExtra("zona")+"%"+intent.getStringExtra("fechahora");


        //String token = intent.getStringExtra("TOKEN");

        TextView folio,operador,eco,fecha,zona,servicio;

        ImageView qr = (ImageView)findViewById(R.id.v_qr);
        Bitmap bitmap = createBarCode(foli,500,500);
        qr.setImageBitmap(bitmap);

        folio=(TextView)findViewById(R.id.view_folio);
        operador=(TextView)findViewById(R.id.v_operador);
        eco = (TextView)findViewById(R.id.v_eco);
        fecha=(TextView)findViewById(R.id.v_date);
        zona = (TextView)findViewById(R.id.v_zona);
        servicio=(TextView)findViewById(R.id.v_servicio);

        folio.setText("FOLIO: "+intent.getStringExtra("folio"));
        operador.setText("OPERADOR: "+intent.getStringExtra("operador"));
        eco.setText("ECO: "+intent.getStringExtra("economico"));
        fecha.setText("FECHA Y HORA: "+intent.getStringExtra("fechahora"));
        zona.setText("ZONA: "+intent.getStringExtra("zona"));
        servicio.setText("SERVICIO: "+intent.getStringExtra("servicio"));


        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

}
